use std::mem;

fn cha_cha_quarter_round(state:&mut [u32; 16], a_index:usize, b_index:usize, c_index:usize, d_index:usize) {
    let mut a = state[a_index];
    let mut b = state[b_index];
    let mut c = state[c_index];
    let mut d = state[d_index];

    a = a.wrapping_add(b);
    d = d ^ a;
    d = d.rotate_left(16);

    c = c.wrapping_add(d);
    b = b ^ c;
    b = b.rotate_left(12);

    a = a.wrapping_add(b);
    d = d ^ a;
    d = d.rotate_left(8);

    c = c.wrapping_add(d);
    b = b ^ c;
    b = b.rotate_left(7);

    state[a_index] = a;
    state[b_index] = b;
    state[c_index] = c;
    state[d_index] = d;
}

fn cha_cha_20_block(key:&[u8;32], counter:u32, nonce:&[u8;12]) -> [u8; 64]{
    let key:[u32;8] = unsafe { mem::transmute(*key)};
    let nonce: [u32; 3] = unsafe { mem::transmute(*nonce) };
    let mut state:[u32;16] = [
        0x61707865, 0x3320646e, 0x79622d32, 0x6b206574,
        key[0], key[1], key[2], key[3],
        key[4], key[5], key[6], key[7],
        counter, nonce[0], nonce[1] , nonce[2]
    ];

    let mut working_state = state;

    for _ in 0usize .. 10 {
        cha_cha_quarter_round(&mut working_state, 0, 4, 8, 12);
        cha_cha_quarter_round(&mut working_state, 1, 5, 9, 13);
        cha_cha_quarter_round(&mut working_state, 2, 6, 10, 14);
        cha_cha_quarter_round(&mut working_state, 3, 7, 11, 15);

        cha_cha_quarter_round(&mut working_state, 0, 5, 10, 15);
        cha_cha_quarter_round(&mut working_state, 1, 6, 11, 12);
        cha_cha_quarter_round(&mut working_state, 2, 7, 8, 13);
        cha_cha_quarter_round(&mut working_state, 3, 4, 9, 14);
    }

    for i in 0usize..16 {
        state[i] = state[i].wrapping_add(working_state[i]);
    }

    unsafe {mem::transmute::<[u32; 16], [u8; 64]>(state)}
}

pub fn cha_cha_20_encrypt(key: &[u8; 32], counter: u32, nonce: &[u8; 12], plain_text:&[u8]) -> Box<[u8]>{
    let mut encrypted_message:Vec<u8> = Vec::with_capacity(plain_text.len());
    let full_block_count = plain_text.len() >> 6;
    for i in 0 ..full_block_count {
        let key_stream = cha_cha_20_block(key, counter + i as u32, nonce);
        let plain_text = &plain_text[i << 6 .. ];
        for c in 0 .. 64 {
            encrypted_message.push(plain_text[c] ^ key_stream[c]);
        }
    }

    let plain_text = &plain_text[full_block_count << 6..];
    let key_stream = cha_cha_20_block(key, counter + full_block_count as u32, nonce);
    for c in 0..plain_text.len() {
        encrypted_message.push(plain_text[c] ^ key_stream[c]);
    }

    encrypted_message.into_boxed_slice()
}

pub fn poly1305_mac(message:&[u8], key:&[u8;32]) -> [u8;16]{
    let mut accumulator: [u32; 5] = [0; 5];
    let (r, s) = unsafe { mem::transmute::<[u8; 32], ([u32;4],[u32;4])>(*key)};
    let r:[u32;4] = [r[0] & 0x0fffffff, r[1] & 0x0ffffffc, r[2] & 0x0ffffffc, r[3] & 0x0ffffffc];
    let mut block: [u32; 4];
    let mut extended_block: [u32; 5];
    let mut byte_block: [u8; 16] = [0; 16];
    let full_block_count = message.len() >> 4;

    for i in 0 .. full_block_count {
        let start = i << 4;
        byte_block.copy_from_slice(&message[start .. start + 16]);
        block = unsafe { mem::transmute (byte_block) };
        extended_block = [block[0], block[1], block[2], block[3], 1];
        accumulator = poly1305_add(accumulator, extended_block);
        poly1305_mul(&mut accumulator, r);
    }

    let message = &message[full_block_count << 4 .. ];
    if message.len() > 0 {
        byte_block = [0; 16];
        for i in 0..message.len() {
            byte_block[i] = message[i];
        }
        byte_block[message.len()] = 0x1u8;
        block = unsafe { mem::transmute(byte_block) };
        extended_block = [block[0], block[1], block[2], block[3], 0];
        accumulator = poly1305_add(accumulator, extended_block);
        poly1305_mul(&mut accumulator, r);
    }
    accumulator = poly1305_mod(accumulator);
    extended_block = [s[0], s[1], s[2], s[3], 0];
    accumulator = poly1305_add(accumulator, extended_block);

    unsafe {mem::transmute([accumulator[0], accumulator[1], accumulator[2], accumulator[3]])}
}


//pub fn poly1305_mac2(mut message: &[u8], key: &[u8; 32]) -> [u8; 16] {
//    let mut accumulator = BigInt::zero();
//    let r = &BigInt::from(&key[..16]) & &BigInt::from(0x0ffffffc0ffffffc0ffffffc0fffffffu128);
//    let s = BigInt::from(&key[16..]);
//    let p = (BigInt::one() <<  130) - BigInt::from(5u64);
//    let mu = BigInt::mu_for_barret_reduction(&p);
//
//    let plus_1 = BigInt::one() << 128;
//    while message.len() >= 16 {
//        let mut n = BigInt::from(&message[..16]);
//        n = &n | &plus_1;
//
//        accumulator = accumulator + &n;
//        accumulator = r.modular_multiplication_with_barret_reduction(&accumulator, &p, &mu);
//        message = &message[16..];
//    }
//    if message.len() > 0 {
//        let mut n = BigInt::from(message);
//        let plus_1 = BigInt::one() << (message.len() << 3);
//        n = &n | &plus_1;
//        accumulator = accumulator + &n;
//        accumulator = r.modular_multiplication(&accumulator, &p);
//    }
//
//    accumulator = accumulator + s;
//    unsafe {mem::transmute([accumulator[0], accumulator[1]])}
//}

// Multiplies h and r, put the result in h
fn poly1305_mul(h:&mut [u32;5], r:[u32;4]){
    // These would fit in 32 bits, but we need 64 bit multiplications
    let r0: u64 = r[0] as u64;
    let r1: u64 = r[1] as u64;
    let r2: u64 = r[2] as u64;
    let r3: u64 = r[3] as u64;

    let h0: u64 = h[0] as u64;
    let h1: u64 = h[1] as u64;
    let h2: u64 = h[2] as u64;
    let h3: u64 = h[3] as u64;
    let h4: u64 = h[4] as u64;

    let rr0 = (r0 >> 2) * 5; // lose 2 bottom bits...
    let rr1 = (r1 >> 2) * 5; // 2 bottom bits already cleared
    let rr2 = (r2 >> 2) * 5; // 2 bottom bits already cleared
    let rr3 = (r3 >> 2) * 5; // 2 bottom bits already cleared

    // school book modular multiplication (without carry propagation)
    let x0:u64 = h0 * r0 + h1 * rr3 + h2 * rr2 + h3 * rr1 + h4 * rr0;
    let x1 = h0 * r1 + h1 * r0  + h2 * rr3 + h3 * rr2 + h4 * rr1;
    let x2 = h0 * r2 + h1 * r1  + h2 * r0  + h3 * rr3 + h4 * rr2;
    let x3 = h0 * r3 + h1 * r2  + h2 * r1  + h3 * r0  + h4 * rr3;
    let x4 = h4 * (r0 & 3); // ...recover those 2 bits

    // carry propagation (put the result back in h)
    let msb:u64 = x4 + (x3 >> 32);
    let mut u = (msb >> 2) * 5; // lose 2 bottom bits...
    u += x0 & 0xffffffff;
    h[0] = (u & 0xffffffff) as u32;
    u >>= 32;
    u += (x1 & 0xffffffff) + (x0 >> 32);
    h[1] = (u & 0xffffffff) as u32;
    u >>= 32;
    u += (x2 & 0xffffffff) + (x1 >> 32);
    h[2] = (u & 0xffffffff) as u32;
    u >>= 32;
    u += (x3 & 0xffffffff) + (x2 >> 32);
    h[3] = (u & 0xffffffff) as u32;
    u >>= 32;
    u += msb & 3 /* ...recover them */;
    h[4] = u as u32;
}

fn poly1305_add(a: [u32; 5], b: [u32; 5]) -> [u32; 5] {
    let mut temp: u64 = 0;
    let mut result: [u32; 5] = Default::default();

    //Compute R = A + B
    for i in 0..5 {
        temp += a[i] as u64 + b[i] as u64;
        result[i] = temp as u32;
        temp >>= 32;
    }

    result
}

fn poly1305_mod(a: [u32; 5]) -> [u32; 5] {
    if a[4] <  3 {
        return a;
    }

    let low_p = 0xfffffffffffffffffffffffffffffffbu128;
    let low_a:u128 = unsafe{ mem::transmute([a[0], a[1], a[2], a[3]])};
    if a[4] == 3 && low_p > low_a{
        return a;
    }
    if a[4] == 3 && low_p == low_a {
        return [0, 0, 0, 0, 0];
    }

    let mut hi_a = a[4];
    let low_result = if low_a >= low_p {
        low_a - low_p
    } else {
        hi_a -= 1;
        !low_p + 1 + low_a
    };

    let low_result:[u32; 4] = unsafe {mem::transmute(low_result)};
    [low_result[0], low_result[1], low_result[2], low_result[3], hi_a - 3]
}

pub fn poly1305_key_gen(key: &[u8; 32], nonce: &[u8; 12]) -> [u8; 32]{
    let counter = 0;
    let block = &cha_cha_20_block(key, counter, nonce)[ .. 32];
    let mut result:[u8; 32] = [0; 32];
    result.copy_from_slice(block);
    result
}

pub fn cha_cha_20_aead_encrypt(aad:&[u8], key:&[u8;32], nonce:[u8;12], plain_text:&[u8]) -> (Box<[u8]>, [u8;16]){
    let otk = poly1305_key_gen(key, &nonce);
    let cipher_text = cha_cha_20_encrypt(key, 1, &nonce, plain_text);
    let mut mac_data:Vec<u8> = Vec::with_capacity(aad.len() + cipher_text.len() + 108);
    mac_data.extend_from_slice(aad);
    pad16(&mut mac_data);

    mac_data.extend_from_slice(&cipher_text);
    pad16(&mut mac_data);

    mac_data.extend_from_slice(&(unsafe { mem::transmute::<u64, [u8; 8]>(aad.len() as u64 )}));
    mac_data.extend_from_slice(&(unsafe { mem::transmute::<u64, [u8; 8]>(cipher_text.len() as u64) }));
    let tag = poly1305_mac(&mac_data, &otk);
    (cipher_text, tag)
}

pub fn cha_cha_20_aead_decrypt(aad: &[u8], key: &[u8; 32], nonce: [u8; 12], cipher_text: &[u8]) -> (Box<[u8]>, [u8; 16]) {
    let otk = poly1305_key_gen(key, &nonce);
    let plain_text = cha_cha_20_encrypt(key, 1, &nonce, cipher_text);
    let mut mac_data: Vec<u8> = Vec::with_capacity(aad.len() + cipher_text.len() + 108);
    mac_data.extend_from_slice(aad);
    pad16(&mut mac_data);

    mac_data.extend_from_slice(&cipher_text);
    pad16(&mut mac_data);

    mac_data.extend_from_slice(&(unsafe { mem::transmute::<u64, [u8; 8]>(aad.len() as u64) }));
    mac_data.extend_from_slice(&(unsafe { mem::transmute::<u64, [u8; 8]>(cipher_text.len() as u64) }));
    let tag = poly1305_mac(&mac_data, &otk);
    (plain_text, tag)
}

#[inline]
fn pad16(data:&mut Vec<u8>){
    for _ in 0 .. (16 - (data.len() & 0xf)) & 0xf {
        data.push(0);
    }
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_cha_cha_quarter_round(){
        let mut state:[u32; 16] = [
            0x879531e0, 0xc5ecf37d, 0x516461b1, 0xc9a62f8a,
            0x44c20ef3, 0x3390af7f, 0xd9fc690b, 0x2a5f714c,
            0x53372767, 0xb00a5631, 0x974c541a, 0x359e9963,
            0x5c971061, 0x3d631689, 0x2098d9d6, 0x91dbd320];

        cha_cha_quarter_round(&mut state, 2, 7, 8, 13);

        assert_eq!(state, [
            0x879531e0, 0xc5ecf37d, 0xbdb886dc, 0xc9a62f8a,
            0x44c20ef3, 0x3390af7f, 0xd9fc690b, 0xcfacafd2,
            0xe46bea80, 0xb00a5631, 0x974c541a, 0x359e9963,
            0x5c971061, 0xccc07c79, 0x2098d9d6, 0x91dbd320]);
    }

    #[test]
    fn test_cha_cha_20_block() {
        let key:[u8; 32] = [0x0, 0x1, 0x2, 0x3, 0x4, 0x5, 0x6, 0x7, 0x8, 0x9, 0xa, 0xb, 0xc, 0xd,
            0xe, 0xf, 0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x1a, 0x1b,
            0x1c, 0x1d, 0x1e, 0x1f];
        let nonce:[u8; 12] = [0x0, 0x0, 0x0, 0x9, 0x0, 0x0, 0x0, 0x4a, 0x0, 0x0, 0x0, 0x0];

        let state:&[u8] = &cha_cha_20_block(&key, 1u32, &nonce);
        let expected:&[u8] = &[
            0x10u8, 0xf1, 0xe7, 0xe4, 0xd1, 0x3b, 0x59, 0x15, 0x50, 0x0f, 0xdd, 0x1f, 0xa3, 0x20, 0x71, 0xc4,
            0xc7u8, 0xd1, 0xf4, 0xc7, 0x33, 0xc0, 0x68, 0x03, 0x04, 0x22, 0xaa, 0x9a, 0xc3, 0xd4, 0x6c, 0x4e,
            0xd2u8, 0x82, 0x64, 0x46, 0x07, 0x9f, 0xaa, 0x09, 0x14, 0xc2, 0xd7, 0x05, 0xd9, 0x8b, 0x02, 0xa2,
            0xb5u8, 0x12, 0x9c, 0xd1, 0xde, 0x16, 0x4e, 0xb9, 0xcb, 0xd0, 0x83, 0xe8, 0xa2, 0x50, 0x3c, 0x4e];

        assert_eq!(state, expected);
    }

    #[test]
    fn test_cha_cha_20_encrypt() {
        let key: [u8; 32] = [0x0, 0x1, 0x2, 0x3, 0x4, 0x5, 0x6, 0x7, 0x8, 0x9, 0xa, 0xb, 0xc, 0xd,
            0xe, 0xf, 0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x1a, 0x1b,
            0x1c, 0x1d, 0x1e, 0x1f];
        let nonce: [u8; 12] = [0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x4a, 0x0, 0x0, 0x0, 0x0];
        let plain_text:&[u8] = &[
            0x4c, 0x61, 0x64, 0x69, 0x65, 0x73, 0x20, 0x61, 0x6e, 0x64, 0x20, 0x47, 0x65, 0x6e, 0x74, 0x6c,
            0x65, 0x6d, 0x65, 0x6e, 0x20, 0x6f, 0x66, 0x20, 0x74, 0x68, 0x65, 0x20, 0x63, 0x6c, 0x61, 0x73,
            0x73, 0x20, 0x6f, 0x66, 0x20, 0x27, 0x39, 0x39, 0x3a, 0x20, 0x49, 0x66, 0x20, 0x49, 0x20, 0x63,
            0x6f, 0x75, 0x6c, 0x64, 0x20, 0x6f, 0x66, 0x66, 0x65, 0x72, 0x20, 0x79, 0x6f, 0x75, 0x20, 0x6f,
            0x6e, 0x6c, 0x79, 0x20, 0x6f, 0x6e, 0x65, 0x20, 0x74, 0x69, 0x70, 0x20, 0x66, 0x6f, 0x72, 0x20,
            0x74, 0x68, 0x65, 0x20, 0x66, 0x75, 0x74, 0x75, 0x72, 0x65, 0x2c, 0x20, 0x73, 0x75, 0x6e, 0x73,
            0x63, 0x72, 0x65, 0x65, 0x6e, 0x20, 0x77, 0x6f, 0x75, 0x6c, 0x64, 0x20, 0x62, 0x65, 0x20, 0x69,
            0x74, 0x2e
        ];

        let cipher_text:&[u8] = &cha_cha_20_encrypt(&key, 1, &nonce, plain_text);
        let expected:&[u8] = &[
            0x6e, 0x2e, 0x35, 0x9a, 0x25, 0x68, 0xf9, 0x80, 0x41, 0xba, 0x07, 0x28, 0xdd, 0x0d, 0x69, 0x81,
            0xe9, 0x7e, 0x7a, 0xec, 0x1d, 0x43, 0x60, 0xc2, 0x0a, 0x27, 0xaf, 0xcc, 0xfd, 0x9f, 0xae, 0x0b,
            0xf9, 0x1b, 0x65, 0xc5, 0x52, 0x47, 0x33, 0xab, 0x8f, 0x59, 0x3d, 0xab, 0xcd, 0x62, 0xb3, 0x57,
            0x16, 0x39, 0xd6, 0x24, 0xe6, 0x51, 0x52, 0xab, 0x8f, 0x53, 0x0c, 0x35, 0x9f, 0x08, 0x61, 0xd8,
            0x07, 0xca, 0x0d, 0xbf, 0x50, 0x0d, 0x6a, 0x61, 0x56, 0xa3, 0x8e, 0x08, 0x8a, 0x22, 0xb6, 0x5e,
            0x52, 0xbc, 0x51, 0x4d, 0x16, 0xcc, 0xf8, 0x06, 0x81, 0x8c, 0xe9, 0x1a, 0xb7, 0x79, 0x37, 0x36,
            0x5a, 0xf9, 0x0b, 0xbf, 0x74, 0xa3, 0x5b, 0xe6, 0xb4, 0x0b, 0x8e, 0xed, 0xf2, 0x78, 0x5e, 0x42,
            0x87, 0x4d];

        assert_eq!(cipher_text, expected);
    }

    #[test]
    fn test_poly1305_mac(){
        let key_material:[u8; 32] = [
            0x85, 0xd6, 0xbe, 0x78, 0x57, 0x55, 0x6d, 0x33, 0x7f, 0x44, 0x52, 0xfe, 0x42, 0xd5,
            0x06, 0xa8, 0x01, 0x03, 0x80, 0x8a, 0xfb, 0x0d, 0xb2, 0xfd, 0x4a, 0xbf, 0xf6, 0xaf,
            0x41, 0x49, 0xf5, 0x1b
        ];
        let message = &[0x43u8, 0x72, 0x79, 0x70, 0x74, 0x6f, 0x67, 0x72, 0x61, 0x70, 0x68,
        0x69, 0x63, 0x20, 0x46, 0x6f, 0x72, 0x75, 0x6d, 0x20, 0x52, 0x65, 0x73, 0x65, 0x61, 0x72,
        0x63, 0x68, 0x20, 0x47, 0x72, 0x6f, 0x75, 0x70];

        let tag:&[u8] = &poly1305_mac(message, &key_material);
        let expected:&[u8] = &[0xa8, 0x06, 0x1d, 0xc1, 0x30, 0x51, 0x36, 0xc6, 0xc2, 0x2b, 0x8b,
            0xaf, 0x0c, 0x01, 0x27, 0xa9];

        assert_eq!(tag, expected);
    }

    #[test]
    fn test_poly1305_key_generation(){
        let key:[u8; 32] = [
            0x80, 0x81, 0x82, 0x83, 0x84, 0x85, 0x86, 0x87, 0x88, 0x89, 0x8a, 0x8b, 0x8c, 0x8d, 0x8e,
            0x8f, 0x90, 0x91, 0x92, 0x93, 0x94, 0x95, 0x96, 0x97, 0x98, 0x99, 0x9a, 0x9b, 0x9c, 0x9d,
            0x9e, 0x9f
        ];

        let nonce:[u8; 12] = [0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07];
        let result:[u8; 32] = poly1305_key_gen(&key, & nonce);
        let expected:[u8; 32] = [
            0x8a, 0xd5, 0xa0, 0x8b, 0x90, 0x5f, 0x81, 0xcc, 0x81, 0x50, 0x40, 0x27, 0x4a, 0xb2, 0x94,
            0x71, 0xa8, 0x33, 0xb6, 0x37, 0xe3, 0xfd, 0x0d, 0xa5, 0x08, 0xdb, 0xb8, 0xe2, 0xfd, 0xd1,
            0xa6, 0x46
        ];

        assert_eq!(&result, &expected);
    }

    #[test]
    fn test_cha_cha_20_aead_encrypt(){
        let plain_text: &[u8] = &[
            0x4c, 0x61, 0x64, 0x69, 0x65, 0x73, 0x20, 0x61, 0x6e, 0x64, 0x20, 0x47, 0x65, 0x6e, 0x74, 0x6c,
            0x65, 0x6d, 0x65, 0x6e, 0x20, 0x6f, 0x66, 0x20, 0x74, 0x68, 0x65, 0x20, 0x63, 0x6c, 0x61, 0x73,
            0x73, 0x20, 0x6f, 0x66, 0x20, 0x27, 0x39, 0x39, 0x3a, 0x20, 0x49, 0x66, 0x20, 0x49, 0x20, 0x63,
            0x6f, 0x75, 0x6c, 0x64, 0x20, 0x6f, 0x66, 0x66, 0x65, 0x72, 0x20, 0x79, 0x6f, 0x75, 0x20, 0x6f,
            0x6e, 0x6c, 0x79, 0x20, 0x6f, 0x6e, 0x65, 0x20, 0x74, 0x69, 0x70, 0x20, 0x66, 0x6f, 0x72, 0x20,
            0x74, 0x68, 0x65, 0x20, 0x66, 0x75, 0x74, 0x75, 0x72, 0x65, 0x2c, 0x20, 0x73, 0x75, 0x6e, 0x73,
            0x63, 0x72, 0x65, 0x65, 0x6e, 0x20, 0x77, 0x6f, 0x75, 0x6c, 0x64, 0x20, 0x62, 0x65, 0x20, 0x69,
            0x74, 0x2e
        ];

        let aad:[u8; 12] = [0x50, 0x51, 0x52, 0x53, 0xc0, 0xc1, 0xc2, 0xc3, 0xc4, 0xc5, 0xc6, 0xc7];
        let key: [u8; 32] = [
            0x80, 0x81, 0x82, 0x83, 0x84, 0x85, 0x86, 0x87, 0x88, 0x89, 0x8a, 0x8b, 0x8c, 0x8d, 0x8e,
            0x8f, 0x90, 0x91, 0x92, 0x93, 0x94, 0x95, 0x96, 0x97, 0x98, 0x99, 0x9a, 0x9b, 0x9c, 0x9d,
            0x9e, 0x9f
        ];

        let iv:[u8; 8] = [0x40, 0x41, 0x42, 0x43, 0x44, 0x45, 0x46, 0x47];
        let constant:[u8; 4] = [0x07, 0x00, 0x00, 0x00];
        let mut constant_and_iv:[u8; 12] = [0; 12];
        constant_and_iv[ .. 4].copy_from_slice(&constant);
        constant_and_iv[4 .. ].copy_from_slice(&iv);


        let expected_cipher_text:&[u8] = &[
            0xd3, 0x1a, 0x8d, 0x34, 0x64, 0x8e, 0x60, 0xdb, 0x7b, 0x86, 0xaf, 0xbc, 0x53, 0xef, 0x7e, 0xc2,
            0xa4, 0xad, 0xed, 0x51, 0x29, 0x6e, 0x08, 0xfe, 0xa9, 0xe2, 0xb5, 0xa7, 0x36, 0xee, 0x62, 0xd6,
            0x3d, 0xbe, 0xa4, 0x5e, 0x8c, 0xa9, 0x67, 0x12, 0x82, 0xfa, 0xfb, 0x69, 0xda, 0x92, 0x72, 0x8b,
            0x1a, 0x71, 0xde, 0x0a, 0x9e, 0x06, 0x0b, 0x29, 0x05, 0xd6, 0xa5, 0xb6, 0x7e, 0xcd, 0x3b, 0x36,
            0x92, 0xdd, 0xbd, 0x7f, 0x2d, 0x77, 0x8b, 0x8c, 0x98, 0x03, 0xae, 0xe3, 0x28, 0x09, 0x1b, 0x58,
            0xfa, 0xb3, 0x24, 0xe4, 0xfa, 0xd6, 0x75, 0x94, 0x55, 0x85, 0x80, 0x8b, 0x48, 0x31, 0xd7, 0xbc,
            0x3f, 0xf4, 0xde, 0xf0, 0x8e, 0x4b, 0x7a, 0x9d, 0xe5, 0x76, 0xd2, 0x65, 0x86, 0xce, 0xc6, 0x4b,
            0x61, 0x16
        ];

        let expected_tag:[u8; 16] = [0x1a, 0xe1, 0x0b, 0x59, 0x4f, 0x09, 0xe2, 0x6a, 0x7e, 0x90,
            0x2e, 0xcb, 0xd0, 0x60, 0x06, 0x91];

        let (cipher_text, tag) = cha_cha_20_aead_encrypt(&aad, &key, constant_and_iv, plain_text);
        let cipher_text:&[u8] = &cipher_text;

        assert_eq!(cipher_text, expected_cipher_text);
        assert_eq!(&tag, &expected_tag);
    }

    #[test]
    fn test_cha_cha_20_aead_decrypt() {
        let expected_plain_text: &[u8] = &[
            0x4c, 0x61, 0x64, 0x69, 0x65, 0x73, 0x20, 0x61, 0x6e, 0x64, 0x20, 0x47, 0x65, 0x6e, 0x74, 0x6c,
            0x65, 0x6d, 0x65, 0x6e, 0x20, 0x6f, 0x66, 0x20, 0x74, 0x68, 0x65, 0x20, 0x63, 0x6c, 0x61, 0x73,
            0x73, 0x20, 0x6f, 0x66, 0x20, 0x27, 0x39, 0x39, 0x3a, 0x20, 0x49, 0x66, 0x20, 0x49, 0x20, 0x63,
            0x6f, 0x75, 0x6c, 0x64, 0x20, 0x6f, 0x66, 0x66, 0x65, 0x72, 0x20, 0x79, 0x6f, 0x75, 0x20, 0x6f,
            0x6e, 0x6c, 0x79, 0x20, 0x6f, 0x6e, 0x65, 0x20, 0x74, 0x69, 0x70, 0x20, 0x66, 0x6f, 0x72, 0x20,
            0x74, 0x68, 0x65, 0x20, 0x66, 0x75, 0x74, 0x75, 0x72, 0x65, 0x2c, 0x20, 0x73, 0x75, 0x6e, 0x73,
            0x63, 0x72, 0x65, 0x65, 0x6e, 0x20, 0x77, 0x6f, 0x75, 0x6c, 0x64, 0x20, 0x62, 0x65, 0x20, 0x69,
            0x74, 0x2e
        ];

        let aad: [u8; 12] = [0x50, 0x51, 0x52, 0x53, 0xc0, 0xc1, 0xc2, 0xc3, 0xc4, 0xc5, 0xc6, 0xc7];
        let key: [u8; 32] = [
            0x80, 0x81, 0x82, 0x83, 0x84, 0x85, 0x86, 0x87, 0x88, 0x89, 0x8a, 0x8b, 0x8c, 0x8d, 0x8e,
            0x8f, 0x90, 0x91, 0x92, 0x93, 0x94, 0x95, 0x96, 0x97, 0x98, 0x99, 0x9a, 0x9b, 0x9c, 0x9d,
            0x9e, 0x9f
        ];

        let iv: [u8; 8] = [0x40, 0x41, 0x42, 0x43, 0x44, 0x45, 0x46, 0x47];
        let constant: [u8; 4] = [0x07, 0x00, 0x00, 0x00];
        let mut constant_and_iv: [u8; 12] = [0; 12];
        constant_and_iv[..4].copy_from_slice(&constant);
        constant_and_iv[4..].copy_from_slice(&iv);


        let cipher_text: &[u8] = &[
            0xd3, 0x1a, 0x8d, 0x34, 0x64, 0x8e, 0x60, 0xdb, 0x7b, 0x86, 0xaf, 0xbc, 0x53, 0xef, 0x7e, 0xc2,
            0xa4, 0xad, 0xed, 0x51, 0x29, 0x6e, 0x08, 0xfe, 0xa9, 0xe2, 0xb5, 0xa7, 0x36, 0xee, 0x62, 0xd6,
            0x3d, 0xbe, 0xa4, 0x5e, 0x8c, 0xa9, 0x67, 0x12, 0x82, 0xfa, 0xfb, 0x69, 0xda, 0x92, 0x72, 0x8b,
            0x1a, 0x71, 0xde, 0x0a, 0x9e, 0x06, 0x0b, 0x29, 0x05, 0xd6, 0xa5, 0xb6, 0x7e, 0xcd, 0x3b, 0x36,
            0x92, 0xdd, 0xbd, 0x7f, 0x2d, 0x77, 0x8b, 0x8c, 0x98, 0x03, 0xae, 0xe3, 0x28, 0x09, 0x1b, 0x58,
            0xfa, 0xb3, 0x24, 0xe4, 0xfa, 0xd6, 0x75, 0x94, 0x55, 0x85, 0x80, 0x8b, 0x48, 0x31, 0xd7, 0xbc,
            0x3f, 0xf4, 0xde, 0xf0, 0x8e, 0x4b, 0x7a, 0x9d, 0xe5, 0x76, 0xd2, 0x65, 0x86, 0xce, 0xc6, 0x4b,
            0x61, 0x16
        ];

        let expected_tag: [u8; 16] = [0x1a, 0xe1, 0x0b, 0x59, 0x4f, 0x09, 0xe2, 0x6a, 0x7e, 0x90,
            0x2e, 0xcb, 0xd0, 0x60, 0x06, 0x91];

        let (plain_text, tag) = cha_cha_20_aead_decrypt(&aad, &key, constant_and_iv, cipher_text);

        assert_eq!(&plain_text as &[u8], expected_plain_text);
        assert_eq!(&tag, &expected_tag);
    }

    #[test]
    fn test_poly1305_mac_2() {
        let otk = [0xfd, 0x72, 0xb9, 0xc9, 0x9a, 0xa1, 0x50, 0x9c, 0xd4, 0x7e, 0x72, 0x27, 0x0a,
            0xc2, 0xbf, 0x07, 0xc9, 0x21, 0x98, 0xb5, 0x77, 0xfc, 0x73, 0xaa, 0x0f, 0x36, 0x3c, 0xf6, 0x38, 0xd3, 0xa6, 0xd7];
        let message:&[u8] = &[0x17, 0x03, 0x03, 0x03, 0xf9, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x6e, 0xc9, 0x75, 0x08, 0x7a, 0x1a, 0x86, 0x19, 0x66, 0x0f, 0x02, 0x13, 0x67,
            0x92, 0x64, 0x11, 0xe8, 0xbb, 0x85, 0x8f, 0xab, 0xd2, 0xad, 0xf5, 0xfe, 0xa9, 0xfe, 0xb5, 0x35, 0x4f, 0x81, 0xc1, 0x21, 0xe3, 0xf3, 0x02, 0xe1, 0x2a, 0xbc, 0x55, 0xa8, 0xd5, 0xa7, 0x08, 0xcd, 0x0e, 0xa9, 0x7d, 0x27, 0x78, 0x8b, 0xe8, 0xf3, 0x7a, 0x6d, 0x44, 0x8c, 0x92, 0xbc, 0x16, 0x13, 0xe5, 0x59, 0x94, 0x9a, 0xde, 0xe0, 0x14, 0x29, 0xd4, 0xf6, 0x44, 0x20, 0xb1, 0xa7, 0xc3, 0xb7, 0x7d, 0xa9, 0x91, 0xd5, 0x2a, 0xa8, 0x0f, 0x90, 0xfc, 0x38, 0x01, 0x27, 0xa4, 0x6b, 0x16, 0xdb, 0x75, 0x0b, 0x06, 0x7a, 0x13, 0xbb, 0xf2, 0x95, 0xbb, 0x2e, 0x07, 0x81, 0x92, 0xe1, 0x05, 0x42, 0xd6, 0x2e, 0xda, 0xb8, 0xb8, 0x65, 0x95, 0xc7, 0x36, 0x1e, 0x68, 0x17, 0x0c, 0xf3, 0x57, 0xdf, 0x2f, 0x60, 0x32, 0x36, 0xdc, 0x57, 0x98, 0x46, 0x22, 0x3c, 0x9f, 0x2d, 0xb6, 0xd2, 0x7c, 0xbf, 0x70, 0xe1, 0x82, 0xb5, 0x2e, 0x92, 0xb3, 0x04, 0x3d, 0x17, 0x81, 0xde, 0x37, 0x59, 0xa4, 0x12, 0xf2, 0x55, 0xf0, 0x95, 0x82, 0x03, 0xaa, 0x52, 0x89, 0xe8, 0x71, 0x70, 0xf7, 0x5a, 0x5f, 0x5c, 0x0d, 0x34, 0x52, 0xc4, 0xb4, 0x65, 0xb6, 0x5f, 0x99, 0xf2, 0x0e, 0x02, 0x1e, 0x24, 0xcb, 0xdc, 0x1d, 0xd2, 0xb7, 0x3c, 0x8a, 0x4a, 0x64, 0x3a, 0x76, 0xa8, 0x99, 0x60, 0x38, 0x78, 0x53, 0x7e, 0x85, 0xf6, 0x20, 0xb7, 0xf4, 0x7d, 0x0a, 0x6c, 0x9c, 0x4d, 0x18, 0xea, 0xa8, 0xdf, 0xf6, 0x18, 0x86, 0xab, 0x5a, 0x8a, 0x9e, 0xdf, 0x13, 0xa0, 0x14, 0x0f, 0xae, 0x33, 0x9c, 0x70, 0x0a, 0x68, 0x24, 0xc3, 0x34, 0x06, 0x08, 0xe4, 0xdc, 0x1d, 0xeb, 0xa9, 0x49, 0xf4, 0x72, 0x9f, 0xd5, 0x4c, 0xaa, 0xa8, 0xf7, 0xc2, 0x3f, 0xf0, 0xd8, 0x0e, 0xe9, 0x1e, 0xb1, 0x21, 0x04, 0x98, 0x3e, 0x4c, 0x8e, 0xa1, 0x5c, 0x1a, 0x69, 0x8e, 0x7a, 0x6c, 0x06, 0xaa, 0x43, 0xbd, 0x5f, 0x56, 0x24, 0x98, 0xd7, 0x12, 0xd3, 0x1c, 0x2e, 0x6c, 0xd9, 0x35, 0x25, 0x28, 0x51, 0xae, 0x4d, 0x62, 0x83, 0xb4, 0x69, 0x36, 0x31, 0xde, 0x8e, 0x92, 0x91, 0xc8, 0xd6, 0x28, 0xad, 0x2e, 0x57, 0xe5, 0x74, 0x99, 0x30, 0xc6, 0x30, 0x35, 0x55, 0x72, 0x7f, 0x0c, 0xed, 0x5b, 0x75, 0x68, 0x75, 0x51, 0xf8, 0x52, 0x52, 0xeb, 0xc2, 0xcc, 0xd7, 0x38, 0xad, 0x3c, 0xb6, 0x23, 0x76, 0xf8, 0xd0, 0x37, 0xb7, 0x73, 0x79, 0x07, 0x8a, 0x4a, 0x3e, 0x81, 0xce, 0xfc, 0x44, 0x9b, 0xbc, 0x0c, 0xf4, 0x7f, 0x51, 0xae, 0x3b, 0x6a, 0xe5, 0x0d, 0x3c, 0x58, 0xf8, 0xbd, 0x98, 0x98, 0xa2, 0xc9, 0x99, 0x44, 0x54, 0x3b, 0x1c, 0x4c, 0x7f, 0x7e, 0xba, 0x85, 0x31, 0x0f, 0x60, 0x72, 0xdf, 0x70, 0x7d, 0xf1, 0x2b, 0xe4, 0xdd, 0x76, 0x27, 0x7d, 0x26, 0x58, 0xe8, 0x50, 0x3b, 0x24, 0xd2, 0xce, 0xf3, 0xb7, 0x34, 0x93, 0xe6, 0xcd, 0x13, 0x82, 0x29, 0xa6, 0x96, 0x01, 0xaf, 0xa9, 0xa8, 0xbf, 0x46, 0x65, 0xbb, 0x24, 0x2b, 0x3b, 0x89, 0x1d, 0xfd, 0x4b, 0x90, 0x93, 0xf6, 0x00, 0xb0, 0x1f, 0x6d, 0x99, 0xdd, 0xb0, 0xc1, 0x92, 0x75, 0x3a, 0xa6, 0xcd, 0x42, 0xaa, 0x38, 0x5d, 0xe6, 0x5d, 0xbd, 0x5d, 0x92, 0x4f, 0x4b, 0x3c, 0x33, 0x7a, 0x6f, 0xc3, 0xc9, 0xc7, 0x06, 0x86, 0x4a, 0x5c, 0xe2, 0xbe, 0xeb, 0x63, 0xf9, 0x16, 0x43, 0xd0, 0x56, 0xcc, 0x3e, 0xc5, 0xa5, 0x78, 0xca, 0xd6, 0x0d, 0xd5, 0x78, 0xac, 0x5a, 0xea, 0x09, 0x68, 0xe9, 0x5a, 0x16, 0x3c, 0x83, 0xf1, 0xbb, 0x34, 0x72, 0xc3, 0x85, 0xf2, 0x51, 0x64, 0x08, 0x74, 0x58, 0xc5, 0xc2, 0xf1, 0xfc, 0x48, 0x55, 0x83, 0x8d, 0x87, 0x87, 0xf6, 0x0c, 0x35, 0xc4, 0x43, 0xe4, 0x98, 0xdf, 0x63, 0x5f, 0xce, 0x8e, 0xac, 0x67, 0x65, 0x1c, 0xc2, 0xd8, 0xac, 0xb0, 0xd9, 0xa2, 0xf5, 0xfd, 0x2f, 0xb5, 0xf2, 0xe5, 0x7e, 0x90, 0xc9, 0x5f, 0x8e, 0x76, 0x48, 0x89, 0xb1, 0x30, 0x83, 0x94, 0x2f, 0x52, 0x7a, 0x00, 0x9b, 0xe0, 0xb2, 0x6c, 0x98, 0xe0, 0x65, 0x50, 0x7d, 0xa1, 0x00, 0xa0, 0xba, 0x9b, 0x38, 0x5d, 0xaa, 0xca, 0x9f, 0x57, 0x0f, 0x4f, 0x12, 0x01, 0x72, 0x87, 0xb6, 0xfb, 0xab, 0x23, 0xf1, 0x93, 0xe8, 0x42, 0xa1, 0xfd, 0x23, 0x64, 0xd8, 0x0b, 0xc6, 0xaf, 0x76, 0x38, 0x13, 0xe2, 0xe8, 0x76, 0x19, 0x5d, 0xdd, 0xe7, 0x7a, 0xe5, 0xf4, 0x5d, 0xe8, 0xf7, 0x98, 0x7b, 0x88, 0x1f, 0xd6, 0x12, 0x0a, 0xbc, 0xa0, 0x6a, 0x5b, 0x26, 0x9c, 0x88, 0x7f, 0x0b, 0xf5, 0x9f, 0x3d, 0x88, 0xf4, 0x9d, 0x5a, 0x76, 0xe9, 0x05, 0x30, 0x01, 0x27, 0x2a, 0x30, 0x5d, 0xf5, 0x55, 0xa1, 0x79, 0xaf, 0xa0, 0x64, 0xf6, 0xad, 0x5a, 0x89, 0x12, 0x56, 0xa1, 0xee, 0x33, 0x4a, 0x96, 0xb7, 0x4f, 0x43, 0xcf, 0x31, 0x00, 0xf5, 0x59, 0xb7, 0xef, 0xea, 0x97, 0xce, 0x77, 0x0b, 0xdf, 0x11, 0xe3, 0xc2, 0xf4, 0x0f, 0x36, 0xc0, 0xbf, 0xf2, 0x44, 0x69, 0x88, 0x82, 0x11, 0xac, 0xe0, 0x79, 0x82, 0x6e, 0x17, 0xef, 0x72, 0x39, 0x08, 0xa4, 0xd9, 0x5a, 0x17, 0x29, 0x6f, 0xcb, 0xb0, 0x66, 0x51, 0xd8, 0xe8, 0xa4, 0x29, 0xe1, 0x6c, 0x05, 0x5e, 0x92, 0x2f, 0x01, 0xfd, 0x70, 0x9c, 0x78, 0xb2, 0xae, 0xdb, 0xb2, 0xbc, 0x1f, 0x0b, 0x75, 0x57, 0xca, 0xac, 0x87, 0x49, 0x26, 0x88, 0x69, 0x90, 0x89, 0xd0, 0x0a, 0x16, 0x9d, 0xae, 0xf6, 0xc7, 0xd2, 0x32, 0x5a, 0x96, 0x6d, 0x34, 0x54, 0xca, 0xb1, 0x09, 0x7a, 0xc2, 0xc1, 0xac, 0x17, 0x54, 0x8d, 0xfe, 0x33, 0xac, 0xf7, 0xe7, 0xc4, 0xc1, 0x74, 0x39, 0xc2, 0x92, 0x88, 0x26, 0x5e, 0x6d, 0x42, 0x6c, 0x5f, 0x7c, 0x4b, 0x1f, 0x72, 0x11, 0xbe, 0xfb, 0x36, 0xfb, 0x85, 0xce, 0xd2, 0x19, 0x6a, 0xe7, 0x65, 0x6e, 0x76, 0x55, 0x96, 0x5a, 0x4e, 0x9e, 0xdc, 0xa9, 0x60, 0x2b, 0x5c, 0x5d, 0x36, 0xca, 0xfb, 0x4f, 0x6b, 0xd9, 0x8d, 0xde, 0x9f, 0xe0, 0x3c, 0xc6, 0xf4, 0xe4, 0xa3, 0x2a, 0x83, 0x33, 0xd0, 0x8b, 0x90, 0xe0, 0x18, 0xa6, 0xa3, 0x08, 0x9b, 0x1d, 0xd2, 0x5b, 0x2d, 0x5b, 0x6b, 0xef, 0x3f, 0x7c, 0xec, 0xbd, 0xe9, 0x43, 0xa9, 0xc0, 0x98, 0xfc, 0x46, 0x49, 0x8e, 0x99, 0x5a, 0xc9, 0xa1, 0x9c, 0xdd, 0xae, 0x7a, 0xd7, 0x30, 0xed, 0xf3, 0x88, 0x47, 0xf5, 0x48, 0xde, 0x2e, 0x79, 0x91, 0xeb, 0x35, 0x60, 0x1e, 0x1b, 0x09, 0x45, 0xfb, 0x87, 0x67, 0xb2, 0x87, 0x94, 0x76, 0x72, 0x48, 0x7b, 0x3f, 0x28, 0x11, 0xdd, 0xe1, 0x07, 0x88, 0x08, 0x75, 0x9f, 0x17, 0x6a, 0x9f, 0xa7, 0x22, 0x63, 0x59, 0x21, 0x61, 0xaa, 0xc2, 0x10, 0xdf, 0x00, 0xfc, 0xef, 0xf7, 0x99, 0xc7, 0x26, 0xcc, 0x5f, 0x8a, 0x19, 0x0b, 0x4f, 0x6c, 0x93, 0x61, 0x87, 0x2a, 0xf2, 0x99, 0x5d, 0xc8, 0x14, 0xbd, 0xa2, 0xa5, 0x8c, 0x21, 0x5e, 0xc3, 0xd9, 0x08, 0xe5, 0xa3, 0x6d, 0xd0, 0x1c, 0xdf, 0xad, 0xb8, 0xc5, 0xd3, 0xd7, 0xa6, 0xf7, 0xc4, 0x7c, 0xd3, 0xf2, 0x3b, 0x55, 0x34, 0x63, 0x1b, 0x64, 0xb5, 0xf1, 0xf2, 0x73, 0x68, 0x6e, 0x99, 0x08, 0x2c, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x05, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xe9, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00];
        let tag = poly1305_mac(message, &otk);
        let expected_tag = &[0x45, 0x6b, 0x78, 0xfc, 0x85, 0x68, 0xbd, 0x62, 0x07, 0x1e, 0x6b, 0x32, 0x9c, 0x23, 0xfb, 0x50];
        assert_eq!(&tag as &[u8], expected_tag);
    }

    #[test]
    fn test_pad16(){
        let mut v:Vec<u8> = vec![];
        pad16(&mut v);
        assert_eq!(v.len(), 0);

        let mut v: Vec<u8> = vec![1];
        pad16(&mut v);
        assert_eq!(v.len(), 16);
    }

    #[test]
    fn test_poly1305_mac_3() {
        let otk = [0x50, 0xd7, 0x07, 0xbc, 0xa8, 0xe5, 0x35, 0x3d, 0x3d, 0xb1, 0x01, 0xf6, 0x78,
            0x10, 0xb7, 0x3f, 0x7d, 0x92, 0x02, 0xd8, 0xe4, 0xbd, 0x11, 0x8f, 0xd7, 0xca, 0x44, 0x68, 0x54, 0x56, 0xe6, 0x6f];
        let message: &[u8] = &[0x17, 0x03, 0x03, 0x05, 0xed, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xde, 0x6c, 0xea, 0x98, 0x72, 0x1c, 0xb8, 0xfc, 0x60, 0xec, 0x26, 0xec, 0x15, 0x50, 0x27, 0x0c, 0x43, 0x29, 0x6b, 0x4c, 0x9a, 0x67, 0x9e, 0xe1, 0x7b, 0x88, 0xfd, 0x73, 0xae, 0xbf, 0xc8, 0x43, 0xd9, 0xcd, 0x74, 0x8c, 0x4e, 0xd2, 0xd9, 0x8d, 0x01, 0x18, 0x63, 0xd9, 0x36, 0xef, 0x81, 0xf6, 0x7a, 0x7e, 0xf0, 0x3a, 0x8f, 0x27, 0xac, 0x81, 0xf1, 0x8d, 0xee, 0xbd, 0x2c, 0xb9, 0xe7, 0x9a, 0xb0, 0x2c, 0xad, 0xe7, 0x98, 0x9d, 0x16, 0x9d, 0x76, 0xda, 0x78, 0xac, 0x58, 0x26, 0x0b, 0x27, 0xd5, 0x3b, 0x66, 0xa4, 0xab, 0x7c, 0x8d, 0x36, 0x2b, 0x9d, 0x12, 0xe3, 0x30, 0x11, 0x9c, 0x49, 0xa6, 0xbc, 0x0a, 0xae, 0x31, 0x7d, 0x6f, 0xa8, 0x24, 0x5c, 0x9f, 0xe7, 0xd6, 0x65, 0x91, 0x72, 0x89, 0xb4, 0xbc, 0x19, 0x16, 0xe2, 0x3b, 0xe4, 0xcb, 0x5d, 0xb1, 0x4a, 0x3b, 0x57, 0x8f, 0x22, 0x2a, 0x6d, 0x87, 0x62, 0x9a, 0xf9, 0x3f, 0x0c, 0x09, 0x7f, 0x0f, 0x80, 0x53, 0x9d, 0x4e, 0x15, 0xb9, 0x29, 0xd5, 0x75, 0x97, 0x9e, 0x01, 0xa5, 0x7c, 0x8a, 0x0b, 0xf2, 0xf2, 0xfb, 0x7d, 0xcd, 0xa5, 0x0d, 0x76, 0x02, 0xd6, 0x6c, 0x43, 0x0b, 0xf2, 0x74, 0xc1, 0x75, 0xc2, 0xc3, 0xe9, 0x25, 0xce, 0xf8, 0x0d, 0xc6, 0x0a, 0xd2, 0xe4, 0xef, 0xa3, 0xb7, 0xed, 0x92, 0x58, 0x51, 0xfc, 0xc6, 0x0d, 0x0e, 0x8f, 0xe1, 0xbe, 0xde, 0x6c, 0x01, 0xb5, 0x03, 0x4f, 0xbb, 0x47, 0x76, 0xf2, 0x04, 0x44, 0xb5, 0x93, 0x83, 0x58, 0xad, 0x06, 0x3e, 0x95, 0x39, 0xbc, 0x16, 0xc9, 0xe7, 0x17, 0x45, 0xce, 0xc0, 0x79, 0x0f, 0x50, 0x74, 0x8d, 0x5a, 0xf6, 0xc4, 0xe2, 0xfd, 0x97, 0xf2, 0x49, 0x86, 0x75, 0x93, 0x8d, 0x9e, 0xbc, 0x23, 0x04, 0x0e, 0x76, 0x2a, 0xf1, 0x66, 0xbd, 0xef, 0xe8, 0xd0, 0xc1, 0xcb, 0xd6, 0x8f, 0x21, 0x6f, 0xfc, 0xd4, 0xd2, 0xed, 0x7b, 0xc4, 0x3a, 0x5d, 0xe7, 0x3c, 0xca, 0xa4, 0x56, 0x18, 0x65, 0x42, 0x5a, 0x78, 0xf4, 0x4a, 0xe1, 0x31, 0xfe, 0x68, 0xfd, 0xd5, 0x85, 0x10, 0xa6, 0x88, 0x45, 0x99, 0xa5, 0x95, 0xd3, 0x3a, 0xe4, 0x6b, 0xd3, 0x03, 0x84, 0xdc, 0xd2, 0x56, 0x01, 0x5c, 0xfc, 0x52, 0x1f, 0x47, 0xca, 0x3a, 0xf6, 0x49, 0xf6, 0x24, 0xc8, 0x4d, 0xfb, 0x69, 0x06, 0x00, 0xd5, 0x12, 0x6b, 0xcb, 0x98, 0xab, 0x51, 0xe5, 0xcc, 0xbf, 0x4d, 0xe9, 0x60, 0x80, 0x8d, 0x41, 0x64, 0x95, 0x36, 0x94, 0x65, 0x4a, 0x04, 0x58, 0xc3, 0xf3, 0x43, 0x54, 0xce, 0x11, 0xe3, 0x95, 0xdb, 0x95, 0xfe, 0x51, 0x08, 0x51, 0x07, 0x37, 0xef, 0x50, 0xca, 0x20, 0x39, 0x2d, 0xce, 0xed, 0x4e, 0x59, 0x68, 0x38, 0x8e, 0x3b, 0xb1, 0xde, 0x21, 0x56, 0xe8, 0xb0, 0x81, 0xb3, 0xcc, 0x66, 0x94, 0x1d, 0xeb, 0x40, 0xe6, 0xb5, 0x2d, 0x79, 0x47, 0x35, 0x4e, 0x85, 0x45, 0xb0, 0x62, 0x4b, 0x01, 0xb7, 0xe4, 0x0b, 0x8f, 0x77, 0x5a, 0x39, 0x32, 0x58, 0xd1, 0xd4, 0xd1, 0xe7, 0xc8, 0x18, 0xd0, 0x75, 0x44, 0x7a, 0x43, 0x41, 0xbe, 0x0e, 0xca, 0xf5, 0x38, 0x9f, 0xdd, 0x1b, 0x47, 0x62, 0x30, 0xee, 0xee, 0x16, 0x7b, 0x32, 0xbc, 0xe3, 0x55, 0x70, 0x0a, 0xcf, 0xe4, 0xf9, 0x65, 0x01, 0xf4, 0x9e, 0x4a, 0x82, 0x68, 0xe0, 0x62, 0x1a, 0xac, 0x14, 0x3c, 0x1b, 0x94, 0x0e, 0xd3, 0x85, 0xa2, 0x1d, 0x61, 0x08, 0xad, 0x5a, 0x9c, 0x5e, 0xdc, 0x81, 0x9c, 0x7b, 0xf2, 0xe4, 0x7f, 0x25, 0x53, 0x47, 0x31, 0x66, 0x4d, 0xbd, 0x80, 0x36, 0x6f, 0xa1, 0x33, 0x80, 0x05, 0x5b, 0x79, 0xe0, 0x86, 0x5f, 0x3c, 0x08, 0xe7, 0x5e, 0x02, 0x7f, 0x42, 0x92, 0xe3, 0x31, 0xf0, 0x6f, 0xbf, 0x91, 0x54, 0x12, 0xa9, 0x6e, 0xe4, 0x48, 0xed, 0x75, 0x78, 0x8f, 0x66, 0x59, 0x00, 0xcd, 0x0a, 0x5a, 0xeb, 0x62, 0xe7, 0x82, 0x61, 0x21, 0xed, 0xc0, 0xd5, 0xc5, 0x08, 0xf6, 0x9a, 0x4c, 0xd0, 0x01, 0x0f, 0xe0, 0xe2, 0x3f, 0xd2, 0xf8, 0x15, 0xc9, 0x91, 0x74, 0xec, 0x9a, 0x36, 0x73, 0x71, 0x1a, 0xb4, 0x8c, 0x3a, 0xe3, 0x31, 0xaf, 0xdd, 0xb8, 0xc2, 0xef, 0x3b, 0x61, 0xe3, 0x6f, 0x1f, 0xea, 0x28, 0xb3, 0x38, 0x77, 0x03, 0x3d, 0x20, 0xa9, 0x79, 0x68, 0x27, 0x68, 0x41, 0x18, 0xea, 0x58, 0xaf, 0xc5, 0x38, 0xae, 0xca, 0x56, 0x81, 0x55, 0xec, 0xa6, 0x59, 0xa0, 0xf0, 0x7a, 0x8d, 0xf2, 0x05, 0x17, 0xd0, 0x93, 0x47, 0x1f, 0x6a, 0x77, 0xc6, 0x60, 0xdb, 0xb5, 0x60, 0x38, 0xe1, 0xd9, 0xf3, 0xc3, 0xae, 0x0a, 0x16, 0x5c, 0x46, 0xd9, 0xdf, 0xf6, 0x8a, 0xa1, 0x6a, 0x09, 0x89, 0xac, 0xaa, 0x53, 0xa0, 0x4c, 0x0e, 0x6b, 0x3b, 0x0c, 0x9c, 0x6d, 0xaa, 0xb4, 0xef, 0x09, 0x00, 0x63, 0x14, 0xa1, 0xda, 0xef, 0x4a, 0x0b, 0xe9, 0xd6, 0x22, 0x51, 0xd5, 0x9b, 0x71, 0xca, 0xd9, 0xbd, 0x43, 0x9b, 0x1e, 0x8b, 0x45, 0x20, 0xb0, 0x94, 0x4a, 0x81, 0x62, 0xc2, 0x0d, 0x48, 0x89, 0xd2, 0x33, 0xf8, 0xed, 0xa8, 0x7d, 0xc1, 0x10, 0x02, 0x57, 0x83, 0x4a, 0x32, 0x1e, 0x35, 0xf3, 0xf4, 0x6c, 0xb3, 0x01, 0x47, 0x3d, 0xe4, 0xb4, 0xc1, 0x7a, 0xfe, 0x19, 0x0f, 0x3a, 0x8c, 0x6f, 0xa7, 0x1d, 0xf0, 0x32, 0x80, 0x7f, 0x92, 0x4c, 0x49, 0xc4, 0xb0, 0x41, 0x08, 0xb2, 0x2f, 0x22, 0x3f, 0x6d, 0xcc, 0xa3, 0xd0, 0x3a, 0xe7, 0x11, 0x14, 0x5a, 0x57, 0x3f, 0xb3, 0xc8, 0x2b, 0x79, 0xe9, 0x53, 0x94, 0x34, 0x74, 0x26, 0x07, 0x85, 0x06, 0x96, 0x4c, 0x93, 0x63, 0x1e, 0x11, 0x18, 0xe8, 0x86, 0x19, 0x2e, 0x82, 0xbe, 0x17, 0x6b, 0x37, 0xba, 0x6d, 0x1a, 0xa0, 0x68, 0x20, 0x54, 0x12, 0xff, 0x88, 0xbc, 0x4b, 0x87, 0x45, 0x9d, 0x03, 0x06, 0x11, 0x57, 0x4d, 0x25, 0x41, 0x3a, 0x5f, 0x11, 0x21, 0x2f, 0xeb, 0xd6, 0xdc, 0x34, 0xda, 0xfd, 0x4c, 0xe0, 0xe9, 0x6c, 0xce, 0x35, 0xed, 0x87, 0xf0, 0x6c, 0xda, 0xb0, 0x77, 0x3a, 0x24, 0x33, 0xea, 0x88, 0xaf, 0x15, 0x98, 0xef, 0xeb, 0xab, 0xc1, 0xc1, 0xf3, 0x8b, 0x9f, 0xbe, 0xdb, 0x34, 0x29, 0x14, 0xf5, 0x9d, 0x58, 0xd1, 0x53, 0x4c, 0xbb, 0xc0, 0x1f, 0xc1, 0x7a, 0x2d, 0x94, 0x2a, 0x86, 0xcc, 0x24, 0x95, 0x42, 0x69, 0xea, 0x72, 0x22, 0x1d, 0xbd, 0x19, 0xd7, 0x5e, 0xb3, 0xd0, 0x5f, 0xb3, 0xa9, 0x4d, 0x52, 0xa0, 0x9a, 0xb0, 0xe3, 0xde, 0x14, 0x2d, 0xf1, 0x3f, 0x82, 0x82, 0x39, 0x9f, 0xee, 0x78, 0xa8, 0xec, 0x33, 0x0c, 0x01, 0x9a, 0xdf, 0xcf, 0xb3, 0x54, 0x02, 0x2e, 0xcc, 0x58, 0x0b, 0xfd, 0x0e, 0xe3, 0xac, 0x12, 0x0f, 0x8b, 0x87, 0x0a, 0xad, 0x1e, 0x4b, 0x1d, 0x45, 0x3e, 0x6c, 0x4c, 0x7d, 0xff, 0x55, 0xb4, 0x81, 0xaa, 0x0b, 0xf0, 0x75, 0x32, 0xa8, 0x32, 0x4d, 0x11, 0xf2, 0xb6, 0x58, 0x42, 0xac, 0x37, 0x6b, 0x78, 0xe3, 0x93, 0x3a, 0xaf, 0x26, 0x59, 0x3c, 0xa2, 0x81, 0xab, 0xbc, 0x2c, 0x42, 0x87, 0xa7, 0x41, 0x55, 0xb1, 0xe2, 0xa3, 0xf1, 0x0a, 0xa8, 0xd8, 0x11, 0xe8, 0xd7, 0xb8, 0x03, 0x44, 0xab, 0xa6, 0x66, 0x98, 0x3c, 0x90, 0x17, 0x88, 0xdd, 0xe7, 0x9c, 0x1a, 0x27, 0xba, 0xce, 0x04, 0x3d, 0x34, 0x74, 0xd1, 0xe1, 0x5b, 0x41, 0x5e, 0x2c, 0x75, 0x30, 0x03, 0xf6, 0x85, 0x17, 0xbb, 0xb8, 0xfb, 0xb7, 0x7a, 0xf9, 0xb7, 0xc2, 0x51, 0x29, 0xe5, 0xc5, 0xf8, 0x0b, 0x86, 0x60, 0x7f, 0x7a, 0x81, 0xe6, 0x0b, 0x23, 0xdf, 0xd2, 0x60, 0x1c, 0xbc, 0xaf, 0x2e, 0x6f, 0x1d, 0xd5, 0x81, 0x5e, 0x0e, 0xd0, 0x07, 0x54, 0x81, 0x29, 0x30, 0xa0, 0x28, 0xa5, 0xe8, 0x48, 0x9d, 0xea, 0x1d, 0xc5, 0x60, 0xff, 0xc3, 0xac, 0x1b, 0x0b, 0xa2, 0xbc, 0x14, 0x63, 0x2b, 0x52, 0xea, 0x5d, 0x8c, 0x48, 0xdb, 0x63, 0x72, 0x4b, 0x1b, 0x2c, 0x27, 0xfd, 0x0d, 0x51, 0xa4, 0x28, 0xec, 0xe2, 0xc2, 0x21, 0xbe, 0x51, 0x77, 0x58, 0x5f, 0x45, 0x81, 0x7e, 0x62, 0x4a, 0x07, 0x5f, 0xd2, 0x2d, 0xa2, 0x4f, 0x18, 0x74, 0x63, 0x1a, 0xae, 0x43, 0x09, 0x5d, 0xfb, 0x1e, 0x5d, 0xfb, 0x5a, 0xff, 0x10, 0x41, 0x92, 0xb9, 0xd4, 0x26, 0x01, 0x1c, 0x45, 0xd1, 0x5c, 0xb1, 0x62, 0x08, 0xad, 0x60, 0x6e, 0xce, 0xdf, 0x27, 0x49, 0x2f, 0xb2, 0x00, 0x59, 0x5b, 0x36, 0x0f, 0xe9, 0xdf, 0xc2, 0xd1, 0xc4, 0x32, 0x84, 0x55, 0x07, 0x22, 0x5d, 0x91, 0x53, 0x9f, 0xef, 0x13, 0xe6, 0x07, 0xb8, 0x85, 0xbb, 0x9f, 0xc7, 0xda, 0x09, 0x3a, 0x68, 0x8d, 0xc9, 0x9f, 0xcd, 0x76, 0xdb, 0x73, 0x6b, 0x57, 0x36, 0x3a, 0xbf, 0xc5, 0x97, 0xcb, 0x85, 0xd1, 0x25, 0xc4, 0x10, 0x07, 0x0c, 0x2c, 0x6f, 0x1e, 0x11, 0x46, 0xc8, 0xbb, 0x10, 0x95, 0x35, 0x1e, 0x39, 0x27, 0x05, 0x6e, 0x91, 0x0d, 0xb5, 0xa6, 0x66, 0xe4, 0xcf, 0xa5, 0xac, 0x4d, 0x2b, 0x48, 0xfd, 0x2d, 0x94, 0x4a, 0x84, 0xde, 0xf6, 0x67, 0x63, 0xa5, 0x4a, 0x53, 0x1d, 0x74, 0x6e, 0xe8, 0xf0, 0xc5, 0x9c, 0x87, 0xec, 0x30, 0xa9, 0x34, 0x3d, 0x89, 0x52, 0x37, 0x84, 0xa9, 0x20, 0x4e, 0xed, 0x89, 0xaa, 0xb3, 0x2e, 0xec, 0x03, 0x38, 0x33, 0xe1, 0x03, 0x35, 0x0a, 0xc7, 0xc0, 0x4c, 0x43, 0x36, 0x8e, 0x38, 0xa8, 0xe9, 0xbc, 0x03, 0x42, 0x86, 0xaf, 0x14, 0xda, 0x1a, 0x20, 0xca, 0xc6, 0xdc, 0xb8, 0xb5, 0xf7, 0x9e, 0x39, 0x7c, 0x9a, 0x43, 0xee, 0xbb, 0x53, 0xe0, 0x6d, 0xa0, 0xef, 0xa7, 0x00, 0xf1, 0x48, 0xc4, 0x7d, 0xb2, 0x60, 0xb2, 0x14, 0xaf, 0x7f, 0x24, 0x83, 0x81, 0x40, 0xff, 0xc2, 0x1a, 0x0d, 0xe8, 0xaf, 0x8c, 0x6d, 0xdf, 0x6d, 0xfc, 0x90, 0x2a, 0x92, 0xb4, 0xd5, 0x21, 0xe5, 0xb4, 0xf2, 0x93, 0xb5, 0x2f, 0x64, 0x45, 0x95, 0xdb, 0x01, 0x87, 0x21, 0x1d, 0xa8, 0x44, 0xcb, 0x26, 0x14, 0x1d, 0xd5, 0x1b, 0xd3, 0x7f, 0xf6, 0x11, 0x4e, 0x01, 0xa8, 0xa3, 0xb8, 0x90, 0xb3, 0x05, 0xf6, 0xc1, 0x00, 0x0d, 0xd5, 0x5a, 0x48, 0xe9, 0x7f, 0x19, 0x43, 0xc4, 0x8b, 0xbb, 0x0b, 0xb2, 0xd0, 0x3c, 0x7f, 0x0d, 0x6c, 0xbd, 0xbe, 0xba, 0xee, 0x40, 0x04, 0x93, 0xf7, 0x1f, 0x97, 0xa9, 0x9b, 0xef, 0x0e, 0x18, 0x0b, 0x82, 0xfe, 0xe5, 0x7b, 0x0a, 0xc5, 0x1b, 0x02, 0x89, 0x4e, 0x6a, 0x62, 0x69, 0x1f, 0x95, 0xa3, 0xb0, 0xeb, 0x87, 0x11, 0xa3, 0x5f, 0xa4, 0x33, 0xbd, 0xeb, 0xd1, 0xc2, 0x91, 0x0c, 0xf8, 0x7f, 0x73, 0xa7, 0x48, 0xdd, 0x1c, 0x32, 0x69, 0x1b, 0xbc, 0xb8, 0x6d, 0x91, 0xc3, 0xb9, 0x59, 0x1b, 0xf1, 0x0d, 0xbe, 0x20, 0x1f, 0xc3, 0x37, 0x91, 0x8c, 0x15, 0xeb, 0xbb, 0xc4, 0xaa, 0xc5, 0x8a, 0xa3, 0xf3, 0x30, 0xad, 0xab, 0x48, 0x5a, 0xb9, 0x4d, 0x08, 0xcc, 0x36, 0x7e, 0x8b, 0x01, 0x0b, 0x01, 0xed, 0xfd, 0xaa, 0x17, 0x50, 0x4f, 0x28, 0x00, 0x00, 0x00, 0x05, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xdd, 0x05, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00];
        let tag = poly1305_mac(message, &otk);
        let expected_tag = &[0x78, 0xfb, 0xe0, 0x05, 0xdb, 0xff, 0x31, 0xf6, 0xdf, 0xdf, 0xe4, 0x58, 0x7b, 0x4d, 0xbe, 0x8e];
        assert_eq!(&tag as &[u8], expected_tag);
    }

    #[test]
    fn test_poly1305_mod() {
        let a: [u32; 5] = [0x2dde68f6, 0x672041f6, 0xf0a01508, 0x1ed7f726, 0x04];
        assert_eq!(poly1305_mod(a), [0x2dde68fb, 0x672041f6, 0xf0a01508, 0x1ed7f726, 0x0]);

        let a: [u32; 5] = [0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0x03];
        assert_eq!(poly1305_mod(a), [0x4, 0, 0, 0, 0]);

        let a: [u32; 5] = [0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0x04];
        assert_eq!(poly1305_mod(a), [0x4, 0, 0, 0, 0x1]);

        let a: [u32; 5] = [0xfffffffa, 0xffffffff, 0xffffffff, 0xffffffff, 0x03];
        assert_eq!(poly1305_mod(a), [0xfffffffa, 0xffffffff, 0xffffffff, 0xffffffff, 0x3]);

        let a: [u32; 5] = [0xfffffffb, 0xffffffff, 0xffffffff, 0xffffffff, 0x03];
        assert_eq!(poly1305_mod(a), [0, 0, 0, 0, 0]);

        let a: [u32; 5] = [0xfffffffc, 0xffffffff, 0xffffffff, 0xffffffff, 0x03];
        assert_eq!(poly1305_mod(a), [1, 0, 0, 0, 0]);
    }
}
