use std::mem;

const CURVE25519_BIT_LEN: usize = 255;
//const CURVE25519_BYTE_LEN: u32 = 32;
//const CURVE25519_WORD_LEN: u32 = 8;
const CURVE25519_A24: u32 = 121666;
pub const U_COORDINATE:[u8; 32] = [9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

struct X25519State
{
    k:[u32;8],
    u:[u32;8],
    x1:[u32;8],
    z1:[u32;8],
    x2:[u32;8],
    z2:[u32;8],
    t1:[u32;8],
    t2:[u32;8],
}

impl Drop for X25519State {
    fn drop(&mut self) {
        let raw = self as *mut X25519State as *mut [u8; 256];
        unsafe {*raw = [0; 256]};
    }
}

pub fn x25519(k: [u8;32], u: [u8; 32]) -> [u8;32] {
    let mut swap: u32 = 0;
    let mut b:u32;
    let mut state = X25519State {
        k: [0; 8],
        u: [0; 8],
        x1: [0; 8],
        z1: [0; 8],
        x2: [0; 8],
        z2: [0; 8],
        t1: [0; 8],
        t2: [0; 8],
    };

    //Copy scalar
    state.k = unsafe { mem::transmute(k)} ;
    //Set the three least significant bits of the first byte and the most
    //significant bit of the last to zero, set the second most significant
    //bit of the last byte to 1
    state.k[0] &= 0xFFFFFFF8;
    state.k[7] &= 0x7FFFFFFF;
    state.k[7] |= 0x40000000;
    //println!("state.k {:x?}", state.k);

    //Copy input u-coordinate
    state.u = unsafe { mem::transmute(u) };

    //Implementations must mask the most significant bit in the final byte
    state.u[7] &= 0x7FFFFFFF;
    //println!("state.u {:x?}", state.u);

    //Implementations must accept non-canonical values and process them as
    //if they had been reduced modulo the field prime (refer to RFC 7748,
    //section 5)
    state.u = curve25519_red(state.u);

    //Set Z1 = 0
    //Set X1 = 1
    state.x1[0] = 1;
    //Set X2 = U
    state.x2 = state.u;
    //Set Z2 = 1
    state.z2[0] = 1;

    //Montgomery ladder
    for i in (0usize .. CURVE25519_BIT_LEN).rev()
    {
        //The scalar is processed in a left-to-right fashion
        b = (state.k[i / 32] >> (i % 32)) &1;


        //Conditional swap
        curve25519_swap(&mut state.x1, &mut state.x2, swap ^ b);
        curve25519_swap(&mut state.z1, &mut state.z2, swap ^ b);

        //Save current bit value
        swap = b;

        //println!("{})state.x1 {:x?}, state.z1 {:x?}, state.x2 {:x?}, state.z2 {:x?}, swap {}", i, state.x1, state.z1, state.x2, state.z2, swap);

        //Compute T1 = X2 + Z2
        state.t1 = curve25519_add(state.x2, state.z2);
        //println!("Compute T1 = X2 + Z2 = {:x?}", state.t1);
        //Compute X2 = X2 - Z2
        state.x2 = curve25519_sub(state.x2, state.z2);
        //Compute Z2 = X1 + Z1
        state.z2 = curve25519_add(state.x1, state.z1);
        //Compute X1 = X1 - Z1
        state.x1 = curve25519_sub(state.x1, state.z1);
        //Compute T1 = T1 * X1
        state.t1 = curve25519_mul(state.t1, state.x1);
        //Compute X2 = X2 * Z2
        state.x2 = curve25519_mul(state.x2, state.z2);
        //Compute Z2 = Z2 * Z2
        state.z2 = curve25519_sqr(state.z2);
        //Compute X1 = X1 * X1
        state.x1 = curve25519_sqr(state.x1);
        //Compute T2 = Z2 - X1
        state.t2 = curve25519_sub(state.z2, state.x1);
        //Compute Z1 = T2 * a24
        state.z1 = curve25519_mul_int(state.t2, CURVE25519_A24);
        //Compute Z1 = Z1 + X1
        state.z1 = curve25519_add(state.z1, state.x1);
        //Compute Z1 = Z1 * T2
        state.z1 = curve25519_mul(state.z1, state.t2);
        //Compute X1 = X1 * Z2
        state.x1 = curve25519_mul(state.x1, state.z2);
        //Compute Z2 = T1 - X2
        state.z2 = curve25519_sub(state.t1, state.x2);
        //Compute Z2 = Z2 * Z2
        state.z2 = curve25519_sqr(state.z2);
        //Compute Z2 = Z2 * U
        state.z2 = curve25519_mul(state.z2, state.u);
        //Compute X2 = X2 + T1
        state.x2 = curve25519_add(state.x2, state.t1);
        //Compute X2 = X2 * X2
        state.x2 = curve25519_sqr(state.x2);
    }

    //Conditional swap
    curve25519_swap(&mut state.x1, &mut state.x2, swap);
    curve25519_swap(&mut state.z1, &mut state.z2, swap);

    //Retrieve affine representation
    state.u = curve25519_inv(state.z1);
    state.u = curve25519_mul(state.u, state.x1);

    unsafe {mem::transmute(state.u)}
}

/**
 * @brief Modular reduction
 * @param[out] r Resulting integer R = A mod p
 * @param[in] a An integer such as 0 <= A < (2 * p)
 **/
fn curve25519_red(a:[u32;8]) -> [u32;8]{
    let mut temp:u64 = 19;
    let mut b:[u32;8] = Default::default();

    //Compute B = A + 19
    for i in 0 .. 8 {
        temp += a[i] as u64;
        b[i] = temp as u32;
        temp >>= 32;
    }

    //Compute B = A - (2^255 - 19)
    b[7] = b[7].wrapping_sub(0x80000000);
    //If B < (2^255 - 19) then R = B, else R = A
    curve25519_select(&b, &a, (b[7] & 0x80000000) >> 31)
}

/**
 * @brief Select an integer
 * @param[out] r Pointer to the destination integer
 * @param[in] a Pointer to the first source integer
 * @param[in] b Pointer to the second source integer
 * @param[in] c Condition variable
 **/
fn curve25519_select(a:&[u32;8], b:&[u32;8], c:u32) -> [u32; 8]{
    //The mask is the all-1 or all-0 word
    let mask = c.wrapping_sub(1);
    let mut r:[u32; 8] = Default::default();
    //Select between A and B
    for i in 0 .. 8 {
        //Constant time implementation
        r[i] = (a[i] & mask) | (b[i] & !mask);
    }

    r
}

/**
 * @brief Conditional swap
 * @param[in,out] a Pointer to the first integer
 * @param[in,out] b Pointer to the second integer
 * @param[in] c Condition variable
 **/
fn curve25519_swap(a:&mut [u32;8], b:&mut [u32;8], c:u32){
    let mut dummy:u32;
    //The mask is the all-1 or all-0 word
    let mask = (!c).wrapping_add(1);

    //Conditional swap
    for i in 0 .. 8 {
        //Constant time implementation
        dummy = mask & (a[i] ^ b[i]);
        a[i] ^= dummy;
        b[i] ^= dummy;
    }
}

/**
 * @brief Modular addition
 * @param[out] r Resulting integer R = (A + B) mod p
 * @param[in] a An integer such as 0 <= A < p
 * @param[in] b An integer such as 0 <= B < p
 **/
fn curve25519_add(a:[u32;8], b:[u32;8]) -> [u32;8]{
    let mut temp:u64 = 0;
    let mut r:[u32; 8] = Default::default();

    //Compute R = A + B
    for i in 0 .. 8 {
        temp += a[i] as u64;
        temp += b[i] as u64;
        r[i] = temp as u32;
        temp >>= 32;
    }

    //Perform modular reduction
    curve25519_red(r)
}

/**
 * @brief Modular subtraction
 * @param[out] r Resulting integer R = (A - B) mod p
 * @param[in] a An integer such as 0 <= A < p
 * @param[in] b An integer such as 0 <= B < p
 **/

fn curve25519_sub(a:[u32;8], b:[u32;8]) -> [u32;8]{
    let mut temp:i64 = -19;
    let mut result:[u32; 8] = Default::default();

    //Compute R = A - 19 - B
    for i in 0 .. 8 {
        temp += a[i] as i64;
        temp -= b[i] as i64;
        result[i] = temp as u32;
        temp >>= 32;
    }

    //Compute R = A + (2^255 - 19) - B
    result[7] = result[7].wrapping_add(0x80000000);

    //Perform modular reduction
    curve25519_red(result)
}

/**
  * @brief Modular multiplication
  * @param[out] r Resulting integer R = (A * B) mod p
  * @param[in] a An integer such as 0 <= A < p
  * @param[in] b An integer such as 0 <= B < p
  **/
fn curve25519_mul(a:[u32;8], b:[u32;8]) -> [u32;8] {
    let mut c:u64 = 0;
    let mut temp:u64 = 0;
    let mut u:[u32;16] = Default::default();

    //Comba's method is used to perform multiplication
    for i in 0 .. 16 {
        //The algorithm computes the products, column by column
        if i < 8 {
            //Inner loop
            for j in 0 ..=i {
                temp += a[j] as u64 * b[i - j] as u64;
                c += temp >> 32;
                temp &= 0xFFFFFFFF;
            }
        }
        else {
            //Inner loop
            for j in i - 7 .. 8 {
                temp += a[j] as u64 * b[i - j] as u64;
                c += temp >> 32;
                temp &= 0xFFFFFFFF;
            }
        }

        //At the bottom of each column, the final result is written to memory
        u[i] = temp as u32;

        //Propagate the carry upwards
        temp = c & 0xFFFFFFFF;
        c >>= 32;
    }

    //Reduce bit 255 (2^255 = 19 mod p)
    temp = (u[7] >> 31) as u64 * 19;
    //Mask the most significant bit
    u[7] &= 0x7FFFFFFF;

    //Perform fast modular reduction (first pass)
    for i in 0 .. 8 {
        temp += u[i] as u64;
        temp += u[i + 8] as u64 * 38;
        u[i] = temp as u32;
        temp >>= 32;
    }

    //Reduce bit 256 (2^256 = 38 mod p)
    temp *= 38;
    //Reduce bit 255 (2^255 = 19 mod p)
    temp += (u[7] >> 31) as u64 * 19;
    //Mask the most significant bit
    u[7] &= 0x7FFFFFFF;

    //Perform fast modular reduction (second pass)
    for i in 0 .. 8 {
        temp += u[i] as u64;
        u[i] = temp as u32;
        temp >>= 32;
    }

    //Reduce non-canonical values
    let mut temp:[u32;8] = Default::default();
    temp.copy_from_slice(&u[..8]);
    curve25519_red(temp)
}

/**
 * @brief Modular squaring
 * @param[out] r Resulting integer R = (A ^ 2) mod p
 * @param[in] a An integer such as 0 <= A < p
 **/
fn curve25519_sqr(a:[u32;8]) -> [u32;8]{
    //Compute R = (A ^ 2) mod p
    curve25519_mul(a, a)
}

/**
 * @brief Modular multiplication
 * @param[out] r Resulting integer R = (A * B) mod p
 * @param[in] a An integer such as 0 <= A < p
 * @param[in] b An integer such as 0 <= B < (2^32 - 1)
 **/
fn curve25519_mul_int(a:[u32;8], b:u32) -> [u32;8]
{
    let mut temp:u64 = 0;
    let mut u:[u32;8] = Default::default();

    //Compute R = A * B
    for i in 0 .. 8 {
        temp += a[i] as u64 * b as u64;
        u[i] = temp as u32;
        temp >>= 32;
    }

    //Reduce bit 256 (2^256 = 38 mod p)
    temp *= 38;
    //Reduce bit 255 (2^255 = 19 mod p)
    temp += (u[7] >> 31) as u64 * 19;
    //Mask the most significant bit
    u[7] &= 0x7FFFFFFF;

    //Perform fast modular reduction
    for i in 0 .. 8 {
        temp += u[i] as u64;
        u[i] = temp as u32;
        temp >>= 32;
    }

    //Reduce non-canonical values
    curve25519_red(u)
}

/**
 * @brief Modular multiplicative inverse
 * @param[out] r Resulting integer R = A^-1 mod p
 * @param[in] a An integer such as 0 <= A < p
 **/
fn curve25519_inv(a:[u32;8]) -> [u32;8]{
    let mut u:[u32;8];
    let mut v:[u32;8];

    //Since GF(p) is a prime field, the Fermat's little theorem can be
    //used to find the multiplicative inverse of A modulo p
    u = curve25519_sqr(a);
    u = curve25519_mul(u, a); //A^(2^2 - 1)
    u = curve25519_sqr(u);
    v = curve25519_mul(u, a); //A^(2^3 - 1)

    u = curve25519_pwr2(v, 3);
    u = curve25519_mul(u, v); //A^(2^6 - 1)
    u = curve25519_sqr(u);
    v = curve25519_mul(u, a); //A^(2^7 - 1)

    u = curve25519_pwr2(v, 7);
    u = curve25519_mul(u, v); //A^(2^14 - 1)
    u = curve25519_sqr(u);
    v = curve25519_mul(u, a); //A^(2^15 - 1)

    u = curve25519_pwr2(v, 15);
    u = curve25519_mul(u, v); //A^(2^30 - 1)
    u = curve25519_sqr(u);
    v = curve25519_mul(u, a); //A^(2^31 - 1)

    u = curve25519_pwr2(v, 31);
    v = curve25519_mul(u, v); //A^(2^62 - 1)

    u = curve25519_pwr2(v, 62);
    u = curve25519_mul(u, v); //A^(2^124 - 1)
    u = curve25519_sqr(u);
    v = curve25519_mul(u, a); //A^(2^125 - 1)

    u = curve25519_pwr2(v, 125);
    u = curve25519_mul(u, v); //A^(2^250 - 1)
    u = curve25519_sqr(u);
    u = curve25519_sqr(u);
    u = curve25519_mul(u, a);
    u = curve25519_sqr(u);
    u = curve25519_sqr(u);
    u = curve25519_mul(u, a);
    u = curve25519_sqr(u);
    curve25519_mul(u, a) //A^(2^255 - 21)
}

/**
  * @brief Raise an integer to power 2^n
  * @param[out] r Resulting integer R = (A ^ (2^n)) mod p
  * @param[in] a An integer such as 0 <= A < p
  * @param[in] n An integer such as n >= 1
  **/
fn curve25519_pwr2(a:[u32;8], n:usize) -> [u32;8]{
    //Pre-compute (A ^ 2) mod p
    let mut result = curve25519_sqr(a);

    //Compute R = (A ^ (2^n)) mod p
    for _ in 1..n  {
        result = curve25519_sqr(result);
    }

    result
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_x25519() {
        let scalar = [0xa5u8, 0x46, 0xe3, 0x6b, 0xf0, 0x52, 0x7c, 0x9d, 0x3b, 0x16, 0x15, 0x4b, 0x82,
            0x46, 0x5e, 0xdd, 0x62, 0x14, 0x4c, 0x0a, 0xc1, 0xfc, 0x5a, 0x18, 0x50, 0x6a, 0x22, 0x44, 0xba, 0x44, 0x9a, 0xc4];
        let u_coordinate = [0xe6u8, 0xdb, 0x68, 0x67, 0x58, 0x30, 0x30, 0xdb, 0x35, 0x94, 0xc1, 0xa4,
            0x24, 0xb1, 0x5f, 0x7c, 0x72, 0x66, 0x24, 0xec, 0x26, 0xb3, 0x35, 0x3b, 0x10, 0xa9, 0x03, 0xa6,
            0xd0, 0xab, 0x1c, 0x4c];
        let result = x25519(scalar, u_coordinate);

        assert_eq!(result, [0xc3, 0xda, 0x55, 0x37, 0x9d, 0xe9, 0xc6, 0x90, 0x8e, 0x94, 0xea, 0x4d,
            0xf2, 0x8d, 0x08, 0x4f, 0x32, 0xec, 0xcf, 0x03, 0x49, 0x1c, 0x71, 0xf7, 0x54, 0xb4, 0x07,
            0x55, 0x77, 0xa2, 0x85, 0x52]);

        let scalar = [0x4b, 0x66, 0xe9, 0xd4, 0xd1, 0xb4, 0x67, 0x3c, 0x5a, 0xd2, 0x26, 0x91, 0x95,
            0x7d, 0x6a, 0xf5, 0xc1, 0x1b, 0x64, 0x21, 0xe0, 0xea, 0x01, 0xd4, 0x2c, 0xa4, 0x16, 0x9e, 0x79,
            0x18, 0xba, 0x0d];
        let u_coordinate = [0xe5, 0x21, 0x0f, 0x12, 0x78, 0x68, 0x11, 0xd3, 0xf4, 0xb7, 0x95, 0x9d,
            0x05, 0x38, 0xae, 0x2c, 0x31, 0xdb, 0xe7, 0x10, 0x6f, 0xc0, 0x3c, 0x3e, 0xfc, 0x4c, 0xd5, 0x49,
            0xc7, 0x15, 0xa4, 0x93];
        let result = x25519(scalar, u_coordinate);

        assert_eq!(result, [0x95, 0xcb, 0xde, 0x94, 0x76, 0xe8, 0x90, 0x7d, 0x7a, 0xad, 0xe4, 0x5c, 0xb4,
            0xb8, 0x73, 0xf8, 0x8b, 0x59, 0x5a, 0x68, 0x79, 0x9f, 0xa1, 0x52, 0xe6, 0xf8, 0xf7, 0x64, 0x7a,
            0xac, 0x79, 0x57]);
    }

    #[test]
    fn test_x25519_series() {
        let  scalar = U_COORDINATE;
        let  u_coordinate = U_COORDINATE;
        let result = x25519(scalar, u_coordinate);

        assert_eq!(result, [0x42, 0x2c, 0x8e, 0x7a, 0x62, 0x27, 0xd7, 0xbc, 0xa1, 0x35, 0x0b, 0x3e, 0x2b,
            0xb7, 0x27, 0x9f, 0x78, 0x97, 0xb8, 0x7b, 0xb6, 0x85, 0x4b, 0x78, 0x3c, 0x60, 0xe8, 0x03, 0x11,
            0xae, 0x30, 0x79]);

        //the following test takes approx. 12 seconds
//        for i in 1 .. 1000 {
//            u_coordinate = scalar;
//            scalar = result;
//            result = x25519(scalar, u_coordinate);
//        }
//
//        assert_eq!(result, [0x68, 0x4c, 0xf5, 0x9b, 0xa8, 0x33, 0x09, 0x55, 0x28, 0x00, 0xef, 0x56, 0x6f,
//            0x2f, 0x4d, 0x3c, 0x1c, 0x38, 0x87, 0xc4, 0x93, 0x60, 0xe3, 0x87, 0x5f, 0x2e, 0xb9, 0x4d, 0x99,
//            0x53, 0x2c, 0x51]);
    }


}