use crate::sha256::encode_core;

const H:[u32; 8] = [0xc1059ed8, 0x367cd507, 0x3070dd17, 0xf70e5939, 0xffc00b31, 0x68581511,
    0x64f98fa7, 0xbefa4fa4];

pub fn encode(bytes: &[u8]) -> [u8; 28] {
    let  h:[u32; 8] = encode_core(bytes, H);
    let mut hh:[u8; 28] = [0; 28];
    for i in 0..7 {
        let start_index = i << 2;
        hh[start_index..start_index + 4].copy_from_slice(&h[i].to_be_bytes());
    }

    hh
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_encode(){
        let result = encode("abc".as_bytes());
        assert_eq!(result, [0x23, 0x09, 0x7D, 0x22, 0x34, 0x05, 0xD8, 0x22, 0x86, 0x42, 0xA4, 0x77, 0xBD, 0xA2, 0x55, 0xB3, 0x2A, 0xAD, 0xBC, 0xE4, 0xBD, 0xA0, 0xB3, 0xF7, 0xE3, 0x6C, 0x9D, 0xA7]);

        let result = encode("abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq".as_bytes());
        assert_eq!(result, [0x75, 0x38, 0x8B, 0x16, 0x51, 0x27, 0x76, 0xCC, 0x5D, 0xBA, 0x5D, 0xA1, 0xFD, 0x89, 0x01, 0x50, 0xB0, 0xC6, 0x45, 0x5C, 0xB4, 0xF5, 0x8B, 0x19, 0x52, 0x52, 0x25, 0x25]);

        let result = encode(&vec!["a".as_bytes()[0]; 1000000]);
        assert_eq!(result, [0x20, 0x79, 0x46, 0x55, 0x98, 0x0C, 0x91, 0xD8, 0xBB, 0xB4, 0xC1, 0xEA, 0x97, 0x61, 0x8A, 0x4B, 0xF0, 0x3F, 0x42, 0x58, 0x19, 0x48, 0xB2, 0xEE, 0x4E, 0xE7, 0xAD, 0x67]);

        let result = encode(vec!["01234567012345670123456701234567"; 20].join("").as_bytes());
        assert_eq!(result, [0x56, 0x7F, 0x69, 0xF1, 0x68, 0xCD, 0x78, 0x44, 0xE6, 0x52, 0x59, 0xCE, 0x65, 0x8F, 0xE7, 0xAA, 0xDF, 0xA2, 0x52, 0x16, 0xE6, 0x8E, 0xCA, 0x0E, 0xB7, 0xAB, 0x82, 0x62]);

        let result = encode(&[0x07]);
        assert_eq!(result, [0x00, 0xEC, 0xD5, 0xF1, 0x38, 0x42, 0x2B, 0x8A, 0xD7, 0x4C, 0x97, 0x99, 0xFD, 0x82, 0x6C, 0x53, 0x1B, 0xAD, 0x2F, 0xCA, 0xBC, 0x74, 0x50, 0xBE, 0xE2, 0xAA, 0x8C, 0x2A]);

        let result = encode(&[0x18, 0x80, 0x40, 0x05, 0xdd, 0x4f, 0xbd, 0x15, 0x56, 0x29, 0x9d, 0x6f, 0x9d, 0x93, 0xdf, 0x62]);
        assert_eq!(result, [0xDF, 0x90, 0xD7, 0x8A, 0xA7, 0x88, 0x21, 0xC9, 0x9B, 0x40, 0xBA, 0x4C, 0x96, 0x69, 0x21, 0xAC, 0xCD, 0x8F, 0xFB, 0x1E, 0x98, 0xAC, 0x38, 0x8E, 0x56, 0x19, 0x1D, 0xB1]);

        let result = encode(&[0x55, 0xb2, 0x10, 0x07, 0x9c, 0x61, 0xb5, 0x3a, 0xdd, 0x52, 0x06, 0x22, 0xd1, 0xac, 0x97, 0xd5,
            0xcd, 0xbe, 0x8c, 0xb3, 0x3a, 0xa0, 0xae, 0x34, 0x45, 0x17, 0xbe, 0xe4, 0xd7, 0xba, 0x09, 0xab,
            0xc8, 0x53, 0x3c, 0x52, 0x50, 0x88, 0x7a, 0x43, 0xbe, 0xbb, 0xac, 0x90, 0x6c, 0x2e, 0x18, 0x37,
            0xf2, 0x6b, 0x36, 0xa5, 0x9a, 0xe3, 0xbe, 0x78, 0x14, 0xd5, 0x06, 0x89, 0x6b, 0x71, 0x8b, 0x2a,
            0x38, 0x3e, 0xcd, 0xac, 0x16, 0xb9, 0x61, 0x25, 0x55, 0x3f, 0x41, 0x6f, 0xf3, 0x2c, 0x66, 0x74,
            0xc7, 0x45, 0x99, 0xa9, 0x00, 0x53, 0x86, 0xd9, 0xce, 0x11, 0x12, 0x24, 0x5f, 0x48, 0xee, 0x47,
            0x0d, 0x39, 0x6c, 0x1e, 0xd6, 0x3b, 0x92, 0x67, 0x0c, 0xa5, 0x6e, 0xc8, 0x4d, 0xee, 0xa8, 0x14,
            0xb6, 0x13, 0x5e, 0xca, 0x54, 0x39, 0x2b, 0xde, 0xdb, 0x94, 0x89, 0xbc, 0x9b, 0x87, 0x5a, 0x8b,
            0xaf, 0x0d, 0xc1, 0xae, 0x78, 0x57, 0x36, 0x91, 0x4a, 0xb7, 0xda, 0xa2, 0x64, 0xbc, 0x07, 0x9d,
            0x26, 0x9f, 0x2c, 0x0d, 0x7e, 0xdd, 0xd8, 0x10, 0xa4, 0x26, 0x14, 0x5a, 0x07, 0x76, 0xf6, 0x7c,
            0x87, 0x82, 0x73]);
        assert_eq!(result, [0x0B, 0x31, 0x89, 0x4E, 0xC8, 0x93, 0x7A, 0xD9, 0xB9, 0x1B, 0xDF, 0xBC, 0xBA, 0x29, 0x4D, 0x9A, 0xDE, 0xFA, 0xA1, 0x8E, 0x09, 0x30, 0x5E, 0x9F, 0x20, 0xD5, 0xC3, 0xA4]);
    }
}