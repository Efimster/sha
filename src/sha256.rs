
const K:[u32; 64] = [
0x428a2f98, 0x71374491, 0xb5c0fbcf, 0xe9b5dba5, 0x3956c25b, 0x59f111f1, 0x923f82a4, 0xab1c5ed5,
0xd807aa98, 0x12835b01, 0x243185be, 0x550c7dc3, 0x72be5d74, 0x80deb1fe, 0x9bdc06a7, 0xc19bf174,
0xe49b69c1, 0xefbe4786, 0x0fc19dc6, 0x240ca1cc, 0x2de92c6f, 0x4a7484aa, 0x5cb0a9dc, 0x76f988da,
0x983e5152, 0xa831c66d, 0xb00327c8, 0xbf597fc7, 0xc6e00bf3, 0xd5a79147, 0x06ca6351, 0x14292967,
0x27b70a85, 0x2e1b2138, 0x4d2c6dfc, 0x53380d13, 0x650a7354, 0x766a0abb, 0x81c2c92e, 0x92722c85,
0xa2bfe8a1, 0xa81a664b, 0xc24b8b70, 0xc76c51a3, 0xd192e819, 0xd6990624, 0xf40e3585, 0x106aa070,
0x19a4c116, 0x1e376c08, 0x2748774c, 0x34b0bcb5, 0x391c0cb3, 0x4ed8aa4a, 0x5b9cca4f, 0x682e6ff3,
0x748f82ee, 0x78a5636f, 0x84c87814, 0x8cc70208, 0x90befffa, 0xa4506ceb, 0xbef9a3f7, 0xc67178f2];

const H:[u32; 8] = [0x6a09e667, 0xbb67ae85, 0x3c6ef372, 0xa54ff53a, 0x510e527f, 0x9b05688c,
    0x1f83d9ab, 0x5be0cd19];

pub fn encode(bytes: &[u8]) -> [u8; 32] {
    let h:[u32; 8] = encode_core(bytes, H);
    let mut hh:[u8; 32] = [0; 32];
    for i in 0..8 {
        let start_index = i << 2;
        hh[start_index..start_index + 4].copy_from_slice(&h[i].to_be_bytes());
    }

    hh
}

pub(crate) fn encode_core(bytes: &[u8], mut h:[u32; 8]) -> [u32; 8] {
    let bytes = get_normalized_message_bytes(bytes);
    //break message into 512 - bit chunks (64 bytes = 2^6)
    for chunk_num in 0..(bytes.len() >> 6) {
        //break chunk into sixteen 32 - bit big - endian words
        let chunk_start_index = chunk_num << 6;
        let chunk: &[u8] = &bytes[chunk_start_index..chunk_start_index + 64];

        let result = process_chunk(chunk, &h);
        for i in 0..8 {
            h[i] = h[i].wrapping_add(result[i]);
        }
    }
    h
}

///begin with the original message of length L bits
///append a single '1' bit
///append K '0' bits, where K is the minimum number >= 0 such that L + 1 + K + 64 is a multiple of 512
///append L as a 64-bit big-endian integer, making the total post-processed length a multiple of 512 bits
fn get_normalized_message_bytes(bytes: &[u8]) -> Vec<u8> {
    let message_length:u64 = bytes.len() as u64;
    let message_bit_length:u64 = message_length << 3;
    let zero_bytes_len = (message_length + 9) % 64;
    let mut bytes: Vec<u8> = bytes.to_vec();
    bytes.push(0x80);

    if zero_bytes_len > 0 {
        for _ in 0.. 64 - zero_bytes_len {
            bytes.push(0);
        }
    }

    bytes.extend_from_slice(&message_bit_length.to_be_bytes());
    bytes
}

fn process_chunk(chunk: &[u8], vars:&[u32]) -> [u32; 8] {
    let (_, chunk, _) = unsafe {chunk.align_to::<u32>()};
    let mut a: u32 = vars[0];
    let mut b: u32 = vars[1];
    let mut c: u32 = vars[2];
    let mut d: u32 = vars[3];
    let mut e: u32 = vars[4];
    let mut f: u32 = vars[5];
    let mut g: u32 = vars[6];
    let mut h: u32 = vars[7];

    //Extend the sixteen 32 - bit words into eighty 32 - bit words:
    let mut w: [u32; 64] = [0; 64];
    for index in 0..chunk.len() {
        w[index] = chunk[index].to_be();
    }

    for num in 16..64 {
        let w_index_minus_15:u32 = w[num - 15];
        let teta_0: u32 = w_index_minus_15.rotate_right(7) ^ w_index_minus_15.rotate_right(18) ^ w_index_minus_15 >> 3;
        let w_index_minus_2 = w[num - 2];
        let teta_1:u32 = w_index_minus_2.rotate_right(17) ^ w_index_minus_2.rotate_right(19) ^ w_index_minus_2 >> 10;

        w[num] = teta_1.wrapping_add(w[num - 7]).wrapping_add(teta_0).wrapping_add(w[num - 16]);
    }

    //main loop
    for num in 0..64 {
        let ch_efg: u32 = (e & f) ^ (!e & g);
        let sum_1e: u32 = e.rotate_right(6) ^ e.rotate_right(11) ^ e.rotate_right(25);
        let temp1 = h.wrapping_add(sum_1e).wrapping_add(ch_efg)
            .wrapping_add(K[num]).wrapping_add(w[num]);

        let maj_abc: u32 = (a & b) ^ (a & c) ^ (b & c);
        let sum_0a: u32 = a.rotate_right(2) ^ a.rotate_right(13) ^ a.rotate_right(22);
        let temp2 = sum_0a.wrapping_add(maj_abc);

        h = g;
        g = f;
        f = e;
        e = d.wrapping_add(temp1);
        d = c;
        c = b;
        b = a;
        a = temp1.wrapping_add(temp2);
    }

    [a, b, c, d, e, f, g, h]
}




#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_encode(){
        let result = encode("12345".as_bytes());
        assert_eq!(result, [0x59, 0x94, 0x47, 0x1a, 0xbb, 0x01, 0x11, 0x2a, 0xfc, 0xc1, 0x81, 0x59, 0xf6,
            0xcc, 0x74, 0xb4, 0xf5, 0x11, 0xb9, 0x98, 0x06, 0xda, 0x59, 0xb3, 0xca, 0xf5, 0xa9, 0xc1,
            0x73, 0xca, 0xcf, 0xc5]);

        let result = encode("".as_bytes());
        assert_eq!(result, [0xe3, 0xb0, 0xc4, 0x42, 0x98, 0xfc, 0x1c, 0x14, 0x9a, 0xfb, 0xf4, 0xc8, 0x99,
            0x6f, 0xb9, 0x24, 0x27, 0xae, 0x41, 0xe4, 0x64, 0x9b, 0x93, 0x4c, 0xa4, 0x95, 0x99, 0x1b, 0x78,
            0x52, 0xb8, 0x55]);
    }

    #[test]
    fn test_encode_2(){
        let text:&[u8] = &[0xcd, 0xa9, 0xfe, 0x30, 0xbf, 0x85, 0x93, 0xe6, 0x1a, 0x05, 0x12, 0x0d, 0xc0, 0xac, 0x2d, 0x2d, 0x16, 0x46, 0x63, 0xbe, 0x91, 0xa2, 0x06, 0x7c, 0x58, 0x47, 0x16, 0x23, 0x68, 0xe9, 0x22, 0xac, 0x36, 0x36, 0x36, 0x36, 0x36, 0x36, 0x36, 0x36, 0x36, 0x36, 0x36, 0x36, 0x36, 0x36, 0x36, 0x36, 0x36, 0x36, 0x36, 0x36, 0x36, 0x36, 0x36, 0x36, 0x36, 0x36, 0x36, 0x36, 0x36, 0x36, 0x36, 0x36, 0x00, 0x20, 0x12, 0x74, 0x6c, 0x73, 0x31, 0x33, 0x20, 0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39, 0x30, 0x31, 0x32, 0x20, 0xda, 0x75, 0xce, 0x11, 0x39, 0xac, 0x80, 0xda, 0xe4, 0x04, 0x4d, 0xa9, 0x32, 0x35, 0x0c, 0xf6, 0x5c, 0x97, 0xcc, 0xc9, 0xe3, 0x3f, 0x1e, 0x6f, 0x7d, 0x2d, 0x4b, 0x18, 0xb7, 0x36, 0xff, 0xd5, 0x01];
        let expected:&[u8] = &[0x60, 0x68, 0x8e, 0xe8, 0x0c, 0x78, 0x8c, 0x98, 0xb5, 0xb7, 0x95, 0x6b, 0x93,
            0x19, 0x8e, 0x9d, 0x8a, 0x40, 0xa9, 0x66, 0xea, 0x70, 0xd3, 0x71, 0x98, 0x0e, 0xb3, 0x3b, 0x3f,
            0x79, 0x2f, 0xa7];
        let result = encode(text);
        assert_eq!(&result, expected);
    }
}

